import random
import sys
sys.path.append("../lists/")
import pprint as pp
from refactor_lists_mp import *


planeInfo = planeInfoSp
planeParts = planePartsSp
weapons = weaponsSp
planesList = ["Random"] + list(planeInfoSp)  # missing random


def generateSpSelection(verbose=False, guiPlane=None, guiWeapon=None):
    curPlane = ""
    curWeapon = ""
    curWeaponList = []
    partsPool = {}
    potentialParts = {}
    selectedParts = []
    guiMessages = {}
    
    if verbose:
        indexnum = 0
        for i in planesList:
            print(indexnum,".",i)
            indexnum += 1
            
    if not guiPlane:  # guiPlane is None: GUI has not been used
        while (curPlane not in range(0,len(planesList))):
            curPlane = input("\nSelect an aircraft by entering its number:\n")
            if curPlane.isdecimal():
                curPlane = int(curPlane)
        if curPlane == 0:
            curPlane = random.choice(planesList[1:])
        else:
            curPlane = planesList[curPlane]
    else:
       if guiPlane == "Random":
            curPlane = random.choice(planesList[1:])
       else:
            curPlane = guiPlane
    
    if verbose:
        print("\nYou have selected:",curPlane,"\n")
    guiMessages["plane_selection"] = curPlane
    
    curWeaponList = ["Random"] + planeInfo[curPlane]["spw"]
    totalSlots = planeInfo[curPlane]["slots"]

    if verbose:
        indexnum = 0
        for i in curWeaponList:
            print(indexnum,".",i)
            indexnum += 1
    
    if not guiWeapon:  # guiWeapon is None: GUI has not been used
        while (curWeapon not in list(range(0,4))):
            curWeapon = input("\nSelect a special weapon by entering its number:\n")
            if curWeapon.isdecimal():
                curWeapon = int(curWeapon)
        if curWeapon == 0:
            curWeapon = random.choice(curWeaponList[1:])
        else:
            curWeapon = curWeaponList[curWeapon]
    else:   # gui has been used
        if guiWeapon == "Random":
            curWeapon = random.choice(curWeaponList[1:])
        else:
            curWeapon = guiWeapon
        
    if verbose:
        print("\nYou have selected:",curWeapon,"\n")
    guiMessages["spw_selection"] = curWeapon
    
    partsPool.update(planeParts["body"])
    partsPool.update(planeParts["msl"])
    partsPool.update(planeParts["misc"])
    if curPlane == "ADF-11F":
        partsPool.update(planeParts["laser"])
    else:
        partsPool.update(planeParts["gun"])
        
    if curWeapon in weapons["spwmsl"]:
        partsPool.update(planeParts["spwmsl"])
    elif curWeapon in weapons["bomb"]:
        partsPool.update(planeParts["bomb"])
        if curWeapon in weapons["tgtbomb"]:
            partsPool.update(planeParts["tgtbomb"])
    elif curWeapon in weapons["laser"]:
        partsPool.update(planeParts["laser"])
    elif curWeapon in weapons["rkt"]:
        partsPool.update(planeParts["rkt"])
        if curWeapon == "GRKT":
            partsPool.update(planeParts["grkt"])
    elif curWeapon == "EML":
        partsPool.update(planeParts["eml"])
    
    remainBodySlots = totalSlots
    remainArmsSlots = totalSlots
    remainMiscSlots = totalSlots
    potentialParts = partsPool.copy()
    for i in range (8):
        for k,v in partsPool.items():
            if k in planeParts["body"] and v > remainBodySlots:
                del potentialParts[k]
            elif k in planeParts["arms"] and v > remainArmsSlots:
                del potentialParts[k]
            elif k in planeParts["misc"] and v > remainMiscSlots:
                del potentialParts[k]
        randomPart, randomPartSlots = random.choice(list(potentialParts.items()))
        selectedParts.append(randomPart)
        if randomPart in planeParts["body"]:
            remainBodySlots = remainBodySlots - randomPartSlots
        elif randomPart in planeParts["arms"]:
            remainArmsSlots = remainArmsSlots - randomPartSlots
        else:
            remainMiscSlots = remainMiscSlots - randomPartSlots
        #print(remainBodySlots,remainArmsSlots,remainMiscSlots)
        del potentialParts[randomPart]
        partsPool = potentialParts.copy()
        # selectedParts = sorted(selectedParts)
        selectedParts.sort(key=lambda x: x[3])
    
    if verbose:
        print("Parts selected:\n")
        print(*selectedParts,"\n", sep="\n",)
    
    # print("body ",remainBodySlots)
    # print("arms ",remainArmsSlots)
    # print("misc ",remainMiscSlots)
    guiMessages["remainingBodySlots"] = remainBodySlots
    guiMessages["remainingArmsSlots"] = remainArmsSlots
    guiMessages["remainingMiscSlots"] = remainMiscSlots
    
    return selectedParts, guiMessages
    
    
def guiExample():
    res, msgs = generateSpSelection(verbose=False, 
        guiPlane="F-15C", guiWeapon="PLSL")
    pp.pprint(res)
    pp.pprint(msgs)
    
    
if __name__ == "__main__":
    restart = ""
    while(restart == ""):
        generateSpSelection(verbose=True)
        restart = input("Press Enter to restart the program. Input anything else to exit.\n")
    # guiExample()
