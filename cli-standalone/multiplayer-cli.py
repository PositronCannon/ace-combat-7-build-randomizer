import random
import sys
sys.path.append("../lists/")
import pprint as pp
from refactor_lists_mp import *


planeInfo = planeInfoMp
planesListUnlimited = planesByCost(99999)
planesList2500 = planesByCost(2500)
planesList2250 = planesByCost(2250)
planesList2000 = planesByCost(2000)
planeParts = planePartsMp
weapons = weaponsMp

costLimitList = ["Random","Unlimited","2500","2250","2000"]

def generateMpSelection(verbose=False, guiCost=None, guiPlane=None, guiWeapon=None, guiPriority=None):
    print("test")
    costLimit = ""
    curPlane = ""
    curWeapon = ""
    curWeaponList = []
    partsPool = {}
    potentialParts = {}
    selectedParts = []
    costWarning = 0
    partsPriority = ""
    guiMessages = {}
    
    ### cost limit selection ###
    
    if verbose:
        indexnum = 0
        for i in costLimitList:
            print(indexnum,".",i)
            indexnum += 1
    
    if not guiCost:  # guiCost is None: GUI has not been used
        while (costLimit not in range(0,5)):
            costLimit = input("Select a cost limit:\n")
            if costLimit.isdecimal():
                costLimit = int(costLimit)
        if costLimit == 0:
            costLimit = random.choice(costLimitList[1:])
        else:  
            costLimit = costLimitList[costLimit]
    else:  # guiCost via GUI
        if guiCost == "Random":
            costLimit = random.choice(costLimitList[1:])
        else:
            costLimit = guiCost
            
    if verbose:
        print("\nYou have selected:",costLimit,"cost limit\n")
    guiMessages["costLimit"] = costLimit
    
    if costLimit == "Unlimited":
        planesList = planesListUnlimited
        costLimit = 100000
    elif costLimit == "2500":
        planesList = planesList2500
    elif costLimit == "2250":
        planesList = planesList2250
    else:
        planesList = planesList2000
    costLimit = int(costLimit)
    
    ### plane selection ###
    
    if verbose:
        indexnum = 0
        for i in planesList:
            print(indexnum,".",i)
            indexnum += 1
    
    if not guiPlane:  # guiPlane is None: GUI has not been used
        while (curPlane not in range(0,len(planesList))):
            curPlane = input("\nSelect an aircraft by entering its number:\n")
            if curPlane.isdecimal():
                curPlane = int(curPlane)
        if curPlane == 0:
            curPlane = random.choice(planesList[1:])
        else:
            curPlane = planesList[curPlane]
        
    else:
        if guiPlane == "Random":
            curPlane = random.choice(planesList[1:])
        else:
            curPlane = guiPlane
    
    if verbose:
        print("\nYou have selected:",curPlane,"\n")
    guiMessages["plane_selection"] = curPlane
    
    curPlaneName = curPlane.split(" (")[0]
    curWeaponList = ['Random'] + planeInfo[curPlaneName]["spw"]
    totalSlots = planeInfo[curPlaneName]["slots"]
    planeCost = planeInfo[curPlaneName]["cost"]
        
    totalCost = planeCost
    remainCost = costLimit - totalCost
    
    ### weapon selection ###
    
    if remainCost < 100:  # todo: put warning in gui
        msg1 = ("The selected plane cannot equip any air-to-air SPW or parts under the selected "
              "cost limit.\n")
        guiMessages["spw_selection"] = 'A2G SPW of choice'
        guiMessages["totalCost"] = totalCost
        guiMessages["remainCost"] = remainCost
        guiMessages["remainingBodySlots"] = 'all'
        guiMessages["remainingArmsSlots"] = 'all'
        guiMessages["remainingMiscSlots"] = 'all'
        guiMessages["warning_under100"] = msg1
        print(msg1)
        return [], guiMessages
    else:
        indexnum = 0
        if verbose:
            for i in curWeaponList:
                print(indexnum,".",i)
                indexnum += 1
        
        if not guiWeapon:  # guiWeapon is None: GUI has not been used
            while (curWeapon not in range(0,len(curWeaponList))):
                # todo: put this info on gui
                curWeapon = input("\nSelect a special weapon by entering its number.\n"
                "Dedicated air-to-ground weapons have a cost of 50. All other weapons have a cost of 100.\n"
                "Selecting a dedicated air-to-ground weapon will exclude SPW parts from selection. "
                "Useful for SPW-off rooms.\n")
                if curWeapon.isdecimal():
                    curWeapon = int(curWeapon)
            if curWeapon == 0:
                curWeapon = random.choice(curWeaponList[1:])
            else:
                curWeapon = curWeaponList[curWeapon]
        else:  # gui has been used
            if guiWeapon == "Random":
                curWeapon = random.choice(curWeaponList[1:])
            else:
                curWeapon = guiWeapon
                
        if verbose:
            print("\nYou have selected:",curWeapon,"\n")
        guiMessages["spw_selection"] = curWeapon
        
        if curWeapon in weapons["a2g"]:
            totalCost += 50
        else:
            totalCost += 100
        remainCost = costLimit - totalCost
        #print(costLimit,totalCost,remainCost)
        
        ### parts selection ###
        
        if not guiPriority:  # guiPriority is None: GUI has not been used
            while partsPriority not in ["y","yes","n","no"]:
                partsPriority = input("Prioritize BODY and ARMS parts over MISC? (y/n)\n")
                partsPriority = partsPriority.lower()
        else:
            partsPriority = guiPriority
                
        partsPool.update(planeParts["body"])
        partsPool.update(planeParts["msl"])
        if partsPriority in ["n","no"]:
            partsPool.update(planeParts["misc"])
        if "ADF-11F" in curPlane:
            partsPool.update(planeParts["laser"])
        else:
            partsPool.update(planeParts["gun"])
        if curWeapon in weapons["spwmsl"]:
            partsPool.update(planeParts["spwmsl"])
        elif curWeapon in weapons["laser"]:
            partsPool.update(planeParts["laser"])
        elif curWeapon == "EML":
            partsPool.update(planeParts["eml"])
        
        remainBodySlots = totalSlots
        remainArmsSlots = totalSlots
        remainMiscSlots = totalSlots
        potentialParts = partsPool.copy()

        for i in range (8):
            for k,v in partsPool.items():
                #print(v)
                partSlots = v['slots']
                partCost = v['cost']
                if k in planeParts["body"] and (partSlots > remainBodySlots or partCost > remainCost):
                    del potentialParts[k]
                if k in planeParts["arms"] and (partSlots > remainArmsSlots or partCost > remainCost):
                    del potentialParts[k]
                if k in planeParts["misc"] and (partSlots > remainMiscSlots or partCost > remainCost):
                    del potentialParts[k]
            if potentialParts == {}:
                if partsPriority in ["y","yes"]:
                    potentialParts.update(planeParts["misc"])
                    partsPool = potentialParts.copy()
                    partsPriority = "n"  # redundant
                    for k,v in partsPool.items():
                        if partSlots > remainMiscSlots or partCost > remainCost:
                            del potentialParts[k]
                            if potentialParts == {}:
                                break
                else:
                    costWarning = 1
                    break
            if potentialParts == {}:
                costWarning = 1
                break
            randomPart = random.choice(list(potentialParts))
            randomPartCost = potentialParts[randomPart]["cost"]
            randomPartSlots = potentialParts[randomPart]["slots"]
            #print("random part info:",randomPart,randomPartCost,randomPartSlots)
            selectedParts.append(randomPart)
            totalCost += randomPartCost
            remainCost = costLimit - totalCost
            #print (totalCost,remainCost)
            if randomPart in planeParts["body"]:
                remainBodySlots -= randomPartSlots
            elif randomPart in planeParts["arms"]:
                remainArmsSlots -= randomPartSlots
            elif randomPart in planeParts["misc"]:
                remainMiscSlots -= randomPartSlots
            #print(remainBodySlots,remainArmsSlots,remainMiscSlots)
            del potentialParts[randomPart]
            partsPool = potentialParts.copy()
            #print(partsPool)
        
        # selectedParts = sorted(selectedParts)
        selectedParts.sort(key=lambda x: x[3])
        
        if verbose:
            print("Parts selected:\n")
            print(*selectedParts,"\n",sep="\n",)
        if costWarning == 1:
            costMsg = "Cost limit reached, no more parts can be selected."
            if verbose:
                print(costMsg + '\n')
            guiMessages["warning_costLimit"] = costMsg
        
        guiMessages["totalCost"] = totalCost
        guiMessages["remainCost"] = remainCost
        guiMessages["remainingBodySlots"] = remainBodySlots
        guiMessages["remainingArmsSlots"] = remainArmsSlots
        guiMessages["remainingMiscSlots"] = remainMiscSlots
        # print("body ",remainBodySlots)
        # print("arms ",remainArmsSlots)
        # print("misc ",remainMiscSlots)
        # print(totalCost,remainCost)
        return selectedParts, guiMessages


def guiExample():
    res, msgs = generateMpSelection(verbose=True, 
        guiCost="Unlimited", guiPlane="F-15C", guiWeapon="PLSL", guiPriority='y')
    #res, msgs = generateMpSelection(verbose=True, 
    #    guiCost="Unlimited", guiPlane="Su-33", guiWeapon="HPAA", guiPriority='y')
    pp.pprint(res)
    pp.pprint(msgs)
        
        
if __name__ == "__main__":
    restart = ""
    while(restart == ""):
        generateMpSelection(verbose=True)
        restart = input("Press Enter to restart the program. Input anything else to exit.\n")
    guiExample()

