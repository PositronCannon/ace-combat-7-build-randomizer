#!/usr/bin/env python 
#-*- coding: utf-8 -*-
import copy, json
import pprint as pp


def planesByCost(costLimit):
    return ["Random"] + [(plane + " (cost: " + str(planeInfoSp[plane]["cost"]) + ")") 
        for plane in planeInfoSp if planeInfoSp[plane]["cost"] <= costLimit - 50]


def checkWeaponMp(weapon):
    a2g_part = [w.split(' ')[0] for w in weaponsMp["a2g"]]
    return weapon + " (MSL/GUN only)" if weapon in a2g_part else weapon


proper_spw_orderSp = ["SAAM","QAAM","HPAA","HCAA","HVAA","LAAM","SASM","4AAM","6AAM","8AAM",
                    "MGP","PLSL","EML","TLS","MPBM","UAV","ADMM","MSTM","IEWS",
                    "LAGM","LASM","LACM","4AGM","8AGM","UGB","SFFS","FAEB",
                    "GPB","XSDB","SOD","RKT","GRKT"]
proper_spw_orderSp_Str = json.dumps(proper_spw_orderSp)
                    
planeInfoSp = {  # {plane: {spw: [], cost: , slots: }
    "F-104C": {"spw": ["SAAM","GRKT","HPAA"], 
        "cost": 1500, "slots": 42},
    "A-10C": {"spw": ["UGB","RKT","4AGM"], 
        "cost": 1750, "slots": 36},
    "F-2A": {"spw": ["LASM","HVAA","RKT"], 
        "cost": 1700, "slots": 38},
    "F-4E": {"spw": ["UGB","LAGM","SASM"], 
        "cost": 1500, "slots": 42},
    "F-14D": {"spw": ["LAAM","GPB","8AAM"], 
        "cost": 1650, "slots": 38},
    "F-15C": {"spw": ["SASM","4AAM","PLSL"], 
        "cost": 1850, "slots": 36},
    "F-15J": {"spw": ["HCAA","SAAM","QAAM"], 
        "cost": 1850, "slots": 36},
    "F-15E": {"spw": ["6AAM","SFFS","TLS"], 
        "cost": 1950, "slots": 34},
    "F-16C": {"spw": ["4AAM","GPB","SASM"], 
        "cost": 1600, "slots": 40},
    "F/A-18F": {"spw": ["QAAM","LASM","EML"], 
        "cost": 1750, "slots": 36},
    "F-22A": {"spw": ["QAAM","XSDB","8AAM"], 
        "cost": 2250, "slots": 30},
    "F-35C": {"spw": ["SOD","4AAM","8AGM"], 
        "cost": 2100, "slots": 32},
    "YF-23": {"spw": ["HVAA","4AAM","UGB"], 
        "cost": 2200, "slots": 32},
    "Mirage 2000-5": {"spw": ["GPB","4AAM","LASM"], 
        "cost": 1700, "slots": 38},
    "Rafale M": {"spw": ["HCAA","LACM","LAAM"], 
        "cost": 1950, "slots": 34},
    "Typhoon": {"spw": ["LAAM","8AGM","HCAA"], 
        "cost": 1950, "slots": 34},
    "Gripen E": {"spw": ["SASM","6AAM","LACM"], 
        "cost": 1750, "slots": 36},
    "MiG-21bis": {"spw": ["MGP","RKT","SAAM"], 
        "cost": 1500, "slots": 42},
    "MiG-29A": {"spw": ["HPAA","LAGM","4AAM"], 
        "cost": 1650, "slots": 38},
    "MiG-31B": {"spw": ["LAAM","SAAM","PLSL"], 
        "cost": 1750, "slots": 36},
    "Su-30M2": {"spw": ["HPAA","4AGM","4AAM"], 
        "cost": 1950, "slots": 34},
    "Su-30SM": {"spw": ["6AAM","QAAM","LAGM"],
        "cost": 2100, "slots": 32},
    "Su-33": {"spw": ["HVAA","LASM","EML"], 
        "cost": 1750, "slots": 36},
    "Su-34": {"spw": ["SFFS","HCAA","4AGM"], 
        "cost": 1950, "slots": 34},
    "Su-35S": {"spw": ["LAAM","6AAM","LASM"], 
        "cost": 2100, "slots": 32},
    "Su-37": {"spw": ["HPAA","4AGM","TLS"], 
        "cost": 1950, "slots": 34},
    "Su-47": {"spw": ["SAAM","UGB","QAAM"], 
        "cost": 2050, "slots": 34},
    "Su-57": {"spw": ["4AAM","GPB","PLSL"], 
        "cost": 2250, "slots": 30},
    "X-02S": {"spw": ["4AAM","LASM","EML"], 
        "cost": 2500, "slots": 28},
    "ADF-11F": {"spw": ["TLS","QAAM","UAV"], 
        "cost": 2500, "slots": 28},
    "ADF-01": {"spw": ["TLS","4AAM","FAEB"], 
        "cost": 2500, "slots": 32},
    "ADFX-01": {"spw": ["TLS","MPBM","IEWS"],
        "cost": 2500, "slots": 28},
    "ASF-X": {"spw": ["6AAM","RKT","LASM"], 
        "cost": 2500, "slots": 30},
    "XFA-27": {"spw": ["SOD","MSTM","IEWS"], 
        "cost": 2500, "slots": 30},
    "CFA-44": {"spw": ["ADMM","EML","IEWS"],
        "cost": 2500, "slots": 30},
    "F-15 S/MTD": {"spw": ["HPAA","FAEB","HVAA"], 
        "cost": 2150, "slots": 34},
    "F-16XL": {"spw": ["UGB","4AAM","4AGM"], 
        "cost": 1750, "slots": 38},
    "FB-22": {"spw": ["XSDB","LAAM","SFFS"], 
        "cost": 2250, "slots": 30},
    "F-2A SK": {"spw": ["LASM","6AAM","IEWS"], 
        "cost": 1800, "slots": 36},
    "F/A-18F Block III": {"spw": ["LACM","8AGM","4AAM"], 
        "cost": 2000, "slots": 34},
    "MiG-35D": {"spw": ["SAAM","6AAM","4AGM"], 
        "cost": 2150, "slots": 32}
}

# {body/arms/gun/msl/spwmsl/bomb/ tgtbomb/laser/eml/rkt/grkt/misc: {slots: }}
planePartsSp = {  
    "body": {
        "[BODY] Light Blisk": 3,
        "[BODY] Cutting-Edge Airbrakes": 2,
        "[BODY] Next Gen High-Pressure Components": 3,
        "[BODY] Variable Cycle Engine": 6,
        "[BODY] ECU Software Update": 5,
        "[BODY] New Flap Actuators": 3,
        "[BODY] New Aileron Actuators": 3,
        "[BODY] New Rudder Actuators": 3,
        "[BODY] Superior Maneuverability": 6,
        "[BODY] Cutting-Edge Large Flap": 5,
        "[BODY] Cutting-Edge Large Ailerons": 4,
        "[BODY] Cutting-Edge Large Rudder": 3,
        "[BODY] Bulletproof Fuel Tank": 4,
        "[BODY] Multipurpose Stealth Coating": 4,
        "[BODY] Automated Fire Extinguisher": 1,
        "[BODY] Queen's Custom": 5
    },
    "arms": {
        "[ARMS] High-Power Enhanced Shells": 7,
        "[ARMS] Long-Range Enhanced Shells": 8,
        "[ARMS] Long-Barrel Enhanced Shells": 5,
        "[ARMS] Directional Proximity Fuze (MSL)": 4,
        "[ARMS] New High-Energy Propellant (MSL)": 8,
        "[ARMS] Takeoff Weight Augmentation (MSL)": 5,
        "[ARMS] Ramjet Propulsion Device (MSL)": 8,
        "[ARMS] Electronic Optical Hybrid Sight (MSL)": 5,
        "[ARMS] Multiple Enemy Detection Device (MSL)": 7,
        "[ARMS] Thrust-Adjusting Steering Device (MSL)": 6,
        "[ARMS] Infrared Seeker Cooling System": 5,
        "[ARMS] Directional Proximity Fuze (Sp. W)": 7,
        "[ARMS] New High-Energy Propellant (Sp. W)": 8,
        "[ARMS] Takeoff Weight Augmentation (Sp. W)": 5,
        "[ARMS] Ramjet Propulsion Device (Sp. W)": 9,
        "[ARMS] Electronic Optical Hybrid Sight (Sp. W)": 7,
        "[ARMS] Multiple Enemy Detection Device (Sp. W)": 5,
        "[ARMS] Thrust-Adjusting Steering Device (Sp. W)": 8,
        "[ARMS] Multiple Target Threat-Level Detection System": 6,
        "[ARMS] High-Spec Explosive Charge": 4,
        "[ARMS] Hardpoint Expansion": 6,
        "[ARMS] GPS Targeting Support System": 8,
        "[ARMS] Additional Bomb Explosives": 7,
        "[ARMS] Additional Bomb Strakes": 8,
        "[ARMS] High-Output Bomb GPS Antenna": 7,
        "[ARMS] High-Output Laser Generator": 7,
        "[ARMS] Laser Beam Expander": 7,
        "[ARMS] Light Condenser Correction Device": 8,
        "[ARMS] High-Power Accumulator": 6,
        "[ARMS] High-Speed Projectiles": 5,
        "[ARMS] EML Sight Correction Algorithm": 7,
        "[ARMS] Additional Rocket Explosives": 6,
        "[ARMS] Additional Rocket Propellant": 6,
        "[ARMS] New GRKT Inertial Navigation Device": 3
    },
    "gun": {
        "[ARMS] High-Power Enhanced Shells": 7,
        "[ARMS] Long-Range Enhanced Shells": 8,
        "[ARMS] Long-Barrel Enhanced Shells": 5
    },
    "msl": {
        "[ARMS] Directional Proximity Fuze (MSL)": 4,
        "[ARMS] New High-Energy Propellant (MSL)": 8,
        "[ARMS] Takeoff Weight Augmentation (MSL)": 5,
        "[ARMS] Ramjet Propulsion Device (MSL)": 8,
        "[ARMS] Electronic Optical Hybrid Sight (MSL)": 5,
        "[ARMS] Multiple Enemy Detection Device (MSL)": 7,
        "[ARMS] Thrust-Adjusting Steering Device (MSL)": 6,
        "[ARMS] Infrared Seeker Cooling System": 5
    },
    "spwmsl": {
        "[ARMS] Directional Proximity Fuze (Sp. W)": 7,
        "[ARMS] New High-Energy Propellant (Sp. W)": 8,
        "[ARMS] Takeoff Weight Augmentation (Sp. W)": 5,
        "[ARMS] Ramjet Propulsion Device (Sp. W)": 9,
        "[ARMS] Electronic Optical Hybrid Sight (Sp. W)": 7,
        "[ARMS] Multiple Enemy Detection Device (Sp. W)": 5,
        "[ARMS] Thrust-Adjusting Steering Device (Sp. W)": 8,
        "[ARMS] Multiple Target Threat-Level Detection System": 6
    },
    "bomb": {
        "[ARMS] High-Spec Explosive Charge": 4,
        "[ARMS] Hardpoint Expansion": 6,
        "[ARMS] GPS Targeting Support System": 8,
        "[ARMS] Additional Bomb Explosives": 7
    },
    "tgtbomb": {
        "[ARMS] Additional Bomb Strakes": 8,
        "[ARMS] High-Output Bomb GPS Antenna": 7
    },
    "laser": {
        "[ARMS] High-Output Laser Generator": 7,
        "[ARMS] Laser Beam Expander": 7,
        "[ARMS] Light Condenser Correction Device": 8
    },    
    "eml": {
        "[ARMS] High-Power Accumulator": 6,
        "[ARMS] High-Speed Projectiles": 5,
        "[ARMS] EML Sight Correction Algorithm": 7
    },
    "rkt": {
        "[ARMS] Additional Rocket Explosives": 6,
        "[ARMS] Additional Rocket Propellant": 6
    },
    "grkt": {
        "[ARMS] New GRKT Inertial Navigation Device": 3
    },
    "misc": {
        "[MISC] Anti-Stealth Microwave Radar": 8,
        "[MISC] Onboard Self-Defense Jammer": 9,
        "[MISC] Main Structural Frame": 2,
        "[MISC] Predictive Lock-On Algorithm": 6,
        "[MISC] Small Anti-Icing/De-Icing System": 7,
        "[MISC] Auto-Strafing Machine Gun Device": 9,
        "[MISC] Machine Gun Radar Lock System": 9
    }
}

weaponsSp = {  # {spwmsl/bomb/tgtbomb/laser/rkt: []}
    "spwmsl": ["SAAM","HPAA","HVAA","HCAA","SASM","LAAM","QAAM","4AAM",
    "6AAM","8AAM","LAGM","LASM","LACM","4AGM","8AGM","MPBM","ADMM","MSTM"],
    "bomb": ["UGB","GPB","SFFS","FAEB","XSDB","SOD"],
    "tgtbomb": ["GPB","XSDB","SOD"],
    "laser": ["PLSL","TLS"],
    "rkt":["RKT","GRKT"]
}

planeListSp = list(planeInfoSp)

allSPWsSp = set()
for plane in planeInfoSp:
    allSPWsSp |= set(planeInfoSp[plane]["spw"])
allSPWsSp = list(allSPWsSp)

planePerSpwSp = {}
for plane in planeInfoSp:
    for weapon in planeInfoSp[plane]["spw"]:
        if weapon in planePerSpwSp:
            planePerSpwSp[weapon] += [plane]
        else:
            planePerSpwSp[weapon] = [plane]
    
spwPerPlaneSp_Str = json.dumps(planeInfoSp)
planePerSpwSp_Str = json.dumps(planePerSpwSp)


# {body/arms/gun/msl/spwmsl/laser/emp/misc: {slots: , cost: }}
planePartsMp = {
    "body": {
        "[BODY] Light Blisk Lv.2":{"slots":5,"cost":40},
        "[BODY] Cutting-Edge Airbrakes Lv.2":{"slots":5,"cost":50},
        "[BODY] Next-Gen High Pressure Components Lv.2":{"slots":6,"cost":50},
        "[BODY] Variable Cycle Engine Lv.2":{"slots":8,"cost":50},
        "[BODY] ECU Software Update Lv.2":{"slots":8,"cost":40},
        "[BODY] New Flap Actuators Lv.3":{"slots":8,"cost":50},
        "[BODY] New Aileron Actuators Lv.3":{"slots":8,"cost":50},
        "[BODY] New Rudder Actuators Lv.3":{"slots":7,"cost":50},
        "[BODY] Superior Maneuverability Lv.2":{"slots":9,"cost":50},
        "[BODY] Cutting-Edge Large Flap Lv.2":{"slots":7,"cost":40},
        "[BODY] Cutting-Edge Large Ailerons Lv.2":{"slots":7,"cost":40},
        "[BODY] Cutting-Edge Large Rudder Lv.2":{"slots":6,"cost":40},
        "[BODY] Bulletproof Fuel Tank Lv.2":{"slots":7,"cost":50},
        "[BODY] Multipurpose Stealth Coating Lv.2":{"slots":7,"cost":50}
    },
    "arms": {
        "[ARMS] High-Power Enhanced Shells Lv.2":{"slots":9,"cost":50},
        "[ARMS] Long-Range Enhanced Shells Lv.2":{"slots":9,"cost":40},
        "[ARMS] Long-Barrel Enhanced Shells Lv.2":{"slots":7,"cost":40},
        "[ARMS] Directional Proximity Fuze (MSL) Lv.2":{"slots":7,"cost":50},
        "[ARMS] New High-Energy Propellant (MSL) Lv.2":{"slots":10,"cost":50},
        "[ARMS] Takeoff Weight Augmentation (MSL) Lv.2":{"slots":7,"cost":40},
        "[ARMS] Ramjet Propulsion Device (MSL) Lv.2":{"slots":10,"cost":40},
        "[ARMS] Electronic Optical Hybrid Sight (MSL) Lv.2":{"slots":7,"cost":50},
        "[ARMS] Multiple Enemy Detection Device (MSL) Lv.2":{"slots":10,"cost":50},
        "[ARMS] Thrust-Adjusting Steering Device (MSL) Lv.2":{"slots":8,"cost":50},
        "[ARMS] Infrared Seeker Cooling System Lv.2":{"slots":7,"cost":40},
        "[ARMS] Directional Proximity Fuze (Sp.W) Lv.2":{"slots":9,"cost":50},
        "[ARMS] New High-Energy Propellant (Sp.W) Lv.2":{"slots":10,"cost":50},
        "[ARMS] Takeoff Weight Augmentation (Sp.W) Lv.2":{"slots":7,"cost":50},
        "[ARMS] Ramjet Propulsion Device (Sp.W) Lv.2":{"slots":11,"cost":40},
        "[ARMS] Electronic Optical Hybrid Sight (Sp.W) Lv.2":{"slots":9,"cost":50},
        "[ARMS] Multiple Enemy Detection Device (Sp.W) Lv.2":{"slots":8,"cost":50},
        "[ARMS] Thrust-Adjusting Steering Device (Sp.W) Lv.2":{"slots":10,"cost":50},
        "[ARMS] Multiple Target Threat-Level Detection System Lv.2":{"slots":8,"cost":40},
        "[ARMS] High-Output Laser Generator Lv.2":{"slots":9,"cost":50},
        "[ARMS] Laser Beam Expander Lv.2":{"slots":9,"cost":50},
        "[ARMS] Light Condenser Correction Device Lv.2":{"slots":10,"cost":50},
        "[ARMS] High-Power Accumulator Lv.2":{"slots":8,"cost":50},
        "[ARMS] High-Speed Projectiles Lv.2":{"slots":7,"cost":40},
        "[ARMS] EML Sight Correction Algorithm Lv.2":{"slots":9,"cost":50}
    },
    "gun": {
        "[ARMS] High-Power Enhanced Shells Lv.2":{"slots":9,"cost":50},
        "[ARMS] Long-Range Enhanced Shells Lv.2":{"slots":9,"cost":40},
        "[ARMS] Long-Barrel Enhanced Shells Lv.2":{"slots":7,"cost":40}
    },
    "msl": {
        "[ARMS] Directional Proximity Fuze (MSL) Lv.2":{"slots":7,"cost":50},
        "[ARMS] New High-Energy Propellant (MSL) Lv.2":{"slots":10,"cost":50},
        "[ARMS] Takeoff Weight Augmentation (MSL) Lv.2":{"slots":7,"cost":40},
        "[ARMS] Ramjet Propulsion Device (MSL) Lv.2":{"slots":10,"cost":40},
        "[ARMS] Electronic Optical Hybrid Sight (MSL) Lv.2":{"slots":7,"cost":50},
        "[ARMS] Multiple Enemy Detection Device (MSL) Lv.2":{"slots":10,"cost":50},
        "[ARMS] Thrust-Adjusting Steering Device (MSL) Lv.2":{"slots":8,"cost":50},
        "[ARMS] Infrared Seeker Cooling System Lv.2":{"slots":7,"cost":40}
    },
    "spwmsl": {
        "[ARMS] Directional Proximity Fuze (Sp.W) Lv.2":{"slots":9,"cost":50},
        "[ARMS] New High-Energy Propellant (Sp.W) Lv.2":{"slots":10,"cost":50},
        "[ARMS] Takeoff Weight Augmentation (Sp.W) Lv.2":{"slots":7,"cost":50},
        "[ARMS] Ramjet Propulsion Device (Sp.W) Lv.2":{"slots":11,"cost":40},
        "[ARMS] Electronic Optical Hybrid Sight (Sp.W) Lv.2":{"slots":9,"cost":50},
        "[ARMS] Multiple Enemy Detection Device (Sp.W) Lv.2":{"slots":8,"cost":50},
        "[ARMS] Thrust-Adjusting Steering Device (Sp.W) Lv.2":{"slots":10,"cost":50},
        "[ARMS] Multiple Target Threat-Level Detection System Lv.2":{"slots":8,"cost":40}
    },
    "laser": {
        "[ARMS] High-Output Laser Generator Lv.2":{"slots":9,"cost":50},
        "[ARMS] Laser Beam Expander Lv.2":{"slots":9,"cost":50},
        "[ARMS] Light Condenser Correction Device Lv.2":{"slots":10,"cost":50}
    },    
    "eml": {
        "[ARMS] High-Power Accumulator Lv.2":{"slots":8,"cost":50},
        "[ARMS] High-Speed Projectiles Lv.2":{"slots":7,"cost":40},
        "[ARMS] EML Sight Correction Algorithm Lv.2":{"slots":9,"cost":50}
    },
    "misc": {
        "[MISC] Anti-Stealth Microwave Radar":{"slots":8,"cost":50},
        "[MISC] Main Structural Frame":{"slots":2,"cost":30},
        "[MISC] Predictive Lock-On Algorithm":{"slots":6,"cost":30},
        "[MISC] Small Anti-Icing/De-Icing System":{"slots":7,"cost":30},
        "[MISC] Auto-Strafing Machine Gun Device":{"slots":9,"cost":20},
        # "[MISC] Machine Gun Radar Lock System":{"slots":9,"cost":30},
        "[MISC] High-Speed Data Link Antenna":{"slots":5,"cost":50},
        "[MISC] Sharp Turn":{"slots":7,"cost":30}
    }
}

weaponsMp = {  # {spwmsl / laser / a2g : []}
    "spwmsl": ["SAAM","HPAA","HVAA","HCAA","SASM","LAAM","QAAM","4AAM",
               "6AAM","8AAM","MPBM","ADMM","MSTM"],
    "laser": ["PLSL","TLS"],
    "a2g": [
        "GRKT (MSL/GUN only)",
        "UGB (MSL/GUN only)",
        "RKT (MSL/GUN only)",
        "4AGM (MSL/GUN only)",
        "LASM (MSL/GUN only)",
        "LAGM (MSL/GUN only)",
        "GPB (MSL/GUN only)",
        "SFFS (MSL/GUN only)",
        "XSDB (MSL/GUN only)",
        "SOD (MSL/GUN only)",
        "8AGM (MSL/GUN only)",
        "LACM (MSL/GUN only)",
        "FAEB (MSL/GUN only)"
    ],
    "MSL/GUN only": ["F-15C", "F-15J", "MiG-31B", "CFA-44"],
    "MSL/PLSL only": ["ADF-11F"]
}

# To make planeInfoMp, append msl/gun only info to a2g weapons
planeInfoMp = copy.deepcopy(planeInfoSp)
for plane in planeInfoSp:
    for spwIdx, spw in enumerate(planeInfoMp[plane]["spw"]):
        spw_with_suffix = spw + " (MSL/GUN only)"
        if spw_with_suffix in weaponsMp['a2g']:
            planeInfoMp[plane]["spw"][spwIdx] += " (MSL/GUN only)"
    if plane in weaponsMp["MSL/GUN only"]:
        planeInfoMp[plane]["spw"].append("MSL/GUN only")
    if plane in weaponsMp["MSL/PLSL only"]:
        planeInfoMp[plane]["spw"].append("MSL/PLSL only")
        
        
planeListMp = list(planeInfoMp)

allSPWsMp = set()
for plane in planeInfoMp:
    allSPWsMp |= set(planeInfoMp[plane]["spw"])
allSPWsMp = list(allSPWsMp)

planePerSpwMp = {}
for plane in planeInfoMp:
    for weapon in planeInfoMp[plane]["spw"]:
        if weapon in planePerSpwMp:
            planePerSpwMp[weapon] += [plane]
        else:
            planePerSpwMp[weapon] = [plane]
    
spwPerPlane_StrMp = json.dumps(planeInfoMp)
planePerSpw_StrMp = json.dumps(planePerSpwMp)
        
proper_spw_orderMp = [checkWeaponMp(w) for w in proper_spw_orderSp] + ["MSL/GUN only", "MSL/PLSL only"]
proper_spw_orderMp_Str = json.dumps(proper_spw_orderMp)
        

