        const spwPerPlaneList = JSON.parse('{{spwPerPlaneSp|safe}}'); // {plane: [SPWs]}
        const planePerSpwList = JSON.parse('{{planePerSpwSp|safe}}'); // {SPW: [planes]}
        const properSpwOrder = JSON.parse('{{proper_spw_orderSp_Str|safe}}'); // spw list
        
        
        function AddSpw() {
            // triggered when plane selection changes
            var planeDropdown = document.getElementById("planes");
            var spwDropdown = document.getElementById("spw");
            var spwSelection = spwDropdown.value;
            var planeSelection = planeDropdown.value;
            
            var randomOption = document.createElement("OPTION");
            randomOption.innerHTML = 'Random';
            randomOption.value = 'Random';
            removeOptions(spwDropdown);
            spwDropdown.options.add(randomOption);
            
            if (planeSelection == 'Random') {
                for (var item of properSpwOrder) {
                    var option = document.createElement("OPTION");
                    option.innerHTML = item;
                    option.value = item;
                    spwDropdown.options.add(option)
                }
            } else {
                for (var x = 0; x < properSpwOrder.length; x++) {
                    if (spwPerPlaneList[planeSelection]["spw"].includes(properSpwOrder[x])) {
                        var option = document.createElement("OPTION");
                        var optionText = properSpwOrder[x]
                        option.innerHTML = optionText;
                        option.value = optionText;
                        spwDropdown.options.add(option);
                    }
                }
                setSelectedValue(spwDropdown, spwSelection);
            }
        }
        
        
        function AddPlane() {
            // triggered when SPW selection changes                
            var planeDropdown = document.getElementById("planes");
            var spwDropdown = document.getElementById("spw");
            var spwSelection = spwDropdown.value;
            var planeSelection = planeDropdown.value;
            
            var randomOption = document.createElement("OPTION");
            randomOption.innerHTML = 'Random';
            randomOption.value = 'Random';
            removeOptions(planeDropdown);
            planeDropdown.options.add(randomOption);
            
            if (spwSelection == 'Random') {
                for (var item of Object.keys(spwPerPlaneList)) {
                    var option = document.createElement("OPTION");
                    option.innerHTML = item;
                    option.value = item;
                    planeDropdown.options.add(option)
                }
            } else {
                for (var i = 0; i < planePerSpwList[spwSelection].length; i++) {
                    var option = document.createElement("OPTION");
                    var optionText = planePerSpwList[spwSelection][i]
                    option.innerHTML = optionText;
                    option.value = optionText;
                    planeDropdown.options.add(option);
                }
                setSelectedValue(planeDropdown, planeSelection);
            }
        }
        
        
        function init() {
            var planeDropdown = document.getElementById("planes");
            var spwDropdown = document.getElementById("spw");
            setSelectedValue(planeDropdown, "Random");
            setSelectedValue(spwDropdown, "Random");
            
            document.getElementById('copy-img').addEventListener('click', CopyToClipboard, true);
        };
        
        document.addEventListener('DOMContentLoaded', init, false);