        function setSelectedValue(selectObj, valueToSet) {
            for (var i = 0; i < selectObj.options.length; i++) {
                if (selectObj.options[i].value == valueToSet) {
                    selectObj.options[i].selected = true;
                    return;
                }
            }
        }
        
        function removeOptions(selectbox) {
            // function empties a dropdown
            var i;
            for (i = selectbox.options.length - 1; i >= 0; i--) {
                selectbox.remove(i);
            }
        }
        
        function CopyToClipboard() {
            var textarea = document.createElement('textarea');
            textarea.textContent = '{{clipboard_text|tojson}}';
            document.body.appendChild(textarea);
            
            var selection = document.getSelection();
            var range = document.createRange();
            //  range.selectNodeContents(textarea);
            range.selectNode(textarea);
            selection.removeAllRanges();
            selection.addRange(range);
            
            console.log('copy success', document.execCommand('copy'));
            selection.removeAllRanges();
            document.body.removeChild(textarea);
        }
        
        
        function RandomizeDropdownSelection(elementId, afterFunction) {
            // get the dropdown element
            var select = document.getElementById(elementId);
            
            // fetch all options within the dropdown
            var options = select.children;
            
            min = Math.ceil(1);
            max = Math.floor(options.length - 1);
            var random = Math.floor(Math.random() * (max - min + 1)) + min;
            
            console.log(random);
            
            // set the value of the dropdown to a random option
            select.value = options[random].value;
            afterFunction();
        }
        