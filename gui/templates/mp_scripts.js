        const spwPerPlaneList = JSON.parse('{{spwPerPlaneMp|safe}}'); // {plane: [SPWs]}
        const planePerSpwList = JSON.parse('{{planePerSpwMp|safe}}'); // {SPW: [planes]}
        const properSpwOrder = JSON.parse('{{proper_spw_orderMp_str|safe}}'); // spw list
        const costList = ["Unlimited", 2500, 2250, 2000];
        
        
        function filterCost(amount) {
            var planeDropdown = document.getElementById("planes");
            var spwDropdown = document.getElementById("spw");
            var planeSelection = planeDropdown.value;
            var spwSelection = spwDropdown.value;
            
            var randomOption = document.createElement("OPTION");
            randomOption.innerHTML = 'Random';
            randomOption.value = 'Random';
            removeOptions(planeDropdown);
            planeDropdown.options.add(randomOption);
            for (var plane of Object.keys(spwPerPlaneList)) {
                if (spwPerPlaneList[plane]["cost"] <= amount - 50) {
                    // what if spw is Random -> not in list for each plane
                    if (spwPerPlaneList[plane]["spw"].includes(spwSelection) | spwSelection == "Random") {
                        var option = document.createElement("OPTION");
                        option.innerHTML = plane + " &ndash; " + spwPerPlaneList[plane]["cost"] + " cost";
                        option.value = plane;
                        planeDropdown.options.add(option);
                    }
                }
                
            }
            setSelectedValue(planeDropdown, planeSelection);
        };
        
        
        function randomizeCost() {
            var randomCost = costList[Math.floor(Math.random() * costList.length)];
            var costButton = document.getElementById("cost_" + randomCost);
            costButton.click();
            costButton.checked = true;
            if (randomCost == "Unlimited") {randomCost = 10000;}
            filterCost(randomCost);
        }
        
        
        function AddSpw() {
            // triggered when plane selection changes
            var planeDropdown = document.getElementById("planes");
            var spwDropdown = document.getElementById("spw");
            var spwSelection = spwDropdown.value;
            var planeSelection = planeDropdown.value;
            
            var randomOption = document.createElement("OPTION");
            randomOption.innerHTML = 'Random';
            randomOption.value = 'Random';
            removeOptions(spwDropdown);
            spwDropdown.options.add(randomOption);
            
            if (planeSelection == 'Random') {
                for (var item of properSpwOrder) {
                    var option = document.createElement("OPTION");
                    option.innerHTML = item;
                    option.value = item;
                    spwDropdown.options.add(option)
                }
            } else {
                for (var spw of properSpwOrder) {
                    if (spwPerPlaneList[planeSelection]["spw"].includes(spw)) {
                        var option = document.createElement("OPTION");
                        option.innerHTML = spw;
                        option.value = spw;
                        spwDropdown.options.add(option);
                    }
                }
                setSelectedValue(spwDropdown, spwSelection);
            }
        }
        
        function AddPlane() {  // also filter by cost
            // triggered when SPW selection changes
            var planeDropdown = document.getElementById("planes");
            var spwDropdown = document.getElementById("spw");
            var spwSelection = spwDropdown.value;
            var planeSelection = planeDropdown.value;
            var selected_cost = settingsForm.elements.namedItem("cost").value;
            var selected_cost_int = 10000;
            
            if (["2500", "2250", "2000"].includes(selected_cost)) {
                selected_cost_int = Number(selected_cost);
                // console.log(selected_cost + ' is in costList: ' + (costList.includes(Number(selected_cost))));
            }
            console.log(selected_cost_int);
            
            var randomOption = document.createElement("OPTION");
            randomOption.innerHTML = 'Random';
            randomOption.value = 'Random';
            removeOptions(planeDropdown);
            planeDropdown.options.add(randomOption);
            
            if (spwSelection == 'Random') {
                for (var plane of Object.keys(spwPerPlaneList)) {
                    if (spwPerPlaneList[plane]["cost"] <= (selected_cost_int - 50)) {
                        var option = document.createElement("OPTION");
                        option.innerHTML = plane + " &ndash; " + spwPerPlaneList[plane]["cost"] + " cost";
                        option.value = plane;
                        planeDropdown.options.add(option);
                    }
                }
            } else {
                for (var plane of planePerSpwList[spwSelection]) {
                    // console.log("plane cost " + spwPerPlaneList[plane]["cost"] + ", limit -50: " + (selected_cost_int - 50) + ' which is ' + (spwPerPlaneList[plane]["cost"] <= (selected_cost_int - 50)));
                    if (spwPerPlaneList[plane]["cost"] <= (selected_cost_int - 50)) {
                        var option = document.createElement("OPTION");
                        option.innerHTML = plane + " &ndash; " + spwPerPlaneList[plane]["cost"] + " cost";
                        option.value = plane;
                        planeDropdown.options.add(option);
                    }
                }
                setSelectedValue(planeDropdown, planeSelection);
            }
        }
        
        
        function init() {
            var planeDropdown = document.getElementById("planes");
            var spwDropdown = document.getElementById("spw");
            setSelectedValue(planeDropdown, "Random");
            setSelectedValue(spwDropdown, "Random");
        
            document.getElementById('copy-img').addEventListener('click', CopyToClipboard, true);
        };
        
        document.addEventListener('DOMContentLoaded', init, false);