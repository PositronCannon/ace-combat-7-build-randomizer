
# coding: utf-8

# ### Import libraries and settings for packaging

# In[ ]:


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from flask import Flask, render_template, request
import json, os, sys
import webbrowser
import random
import importlib  
import pprint

sys.path.append("../cli-standalone/")
sp = importlib.import_module("campaign-cli")
mp = importlib.import_module("multiplayer-cli")
rl = importlib.import_module("refactor_lists_mp")
pp = pprint.PrettyPrinter(indent=4)


# for jupyter
# from IPython.core.display import HTML
#import requests
#import threading
#from IPython.core.interactiveshell import InteractiveShell
#InteractiveShell.ast_node_interactivity = "all"

#app.debug = True


def open_browser():
    webbrowser.open_new('http://127.0.0.1:5000/')


# ### Produces response for home page [127.0.0.1:5000/](http://127.0.0.1:5000/):
# 
# To produce the results, the GET request takes the format:  
# `http://127.0.0.1:5000/results?planes=`**`{plane}`**`&spw=`**`{spw}`**`&placeholder_field=`**`{anything}`**``
# 
# e.g. 
# 
# * http://127.0.0.1:5000/?planes=F-2A&spw=LASM&placeholder_field=test or  
# * http://127.0.0.1:5000/?planes=F-2A&spw=Random&placeholder_field=test or  
# * http://127.0.0.1:5000/?planes=Random&spw=Random&placeholder_field=test
# 
# * http://127.0.0.1:5000/?planes=A-10C&spw=TPS

# In[ ]:


# to remake app every time
if getattr(sys, 'frozen', False):
    template_folder = os.path.join(sys._MEIPASS, 'templates')
    static_folder = os.path.join(sys._MEIPASS, 'static')
    app = Flask(__name__, template_folder=template_folder, static_folder=static_folder)
else:
    app = Flask(__name__)


# response for home
@app.route('/', methods=['POST', 'GET'])
def home():
    plane_selected = request.args.get('planes', 'Random')
    spw_selected = request.args.get('spw', 'Random')
    main_selection = {}
    clipboard_info = []
    plane_selected_before = plane_selected
    print(plane_selected, spw_selected)

    valid_plane = True
    valid_spw = True
    
    if plane_selected not in rl.planeListSp + ['Random']:
        valid_plane = False
    if spw_selected not in rl.allSPWsSp + ['Random']:
        valid_spw = False
        
    if not valid_plane and not valid_spw:
        return render_template('bad.html', errorMsg='Invalid aircraft and SPW selection.')
    if valid_plane and not valid_spw:
        return render_template('bad.html', errorMsg='Invalid SPW selection.')
    if not valid_plane and valid_spw:
        return render_template('bad.html', errorMsg='Invalid aircraft selection.')
    
    if valid_plane and valid_spw:
        if 'Random' not in [plane_selected, spw_selected]:
            if not (spw_selected in sp.planeInfo[plane_selected]["spw"]):
                return render_template('bad.html', errorMsg='Incompatible aircraft and SPW selection.')
                
        else:  # if plane selected == 'Random'
            if spw_selected != 'Random':
                plane_selected = random.choice(rl.planePerSpwSp[spw_selected])

    partList, msgList = sp.generateSpSelection(verbose=False, 
                                               guiPlane=plane_selected, guiWeapon=spw_selected)

    plane_final, spw_final = msgList["plane_selection"], msgList["spw_selection"]
    main_selection = {'Aircraft': plane_final, 'SPW': spw_final}
    if plane_selected_before == 'Random':
        main_selection['Aircraft'] += ' (Random)'
    if spw_selected == 'Random':
        main_selection['SPW'] += ' (Random)'
    
    clipboard_info.append('Aircraft:\\t' + main_selection['Aircraft'])
    clipboard_info.append('SPW:\\t' + main_selection['SPW'])
    partList.sort(key=lambda x:x[3])
    for idx, part in enumerate(partList):
        clipboard_info.append('Part ' + (str(idx + 1)) + ':\\t' + part)

    clipboard_text = '\\n'.join(clipboard_info)
    return render_template('singleplayer.html', planesList=rl.planeListSp, 
                           proper_spw_order=rl.proper_spw_orderSp, 
                           planePerSpwSp=rl.planePerSpwSp_Str, spwPerPlaneSp=rl.spwPerPlaneSp_Str,
                           proper_spw_orderSp_Str=rl.proper_spw_orderSp_Str,
                           parts=partList, main_selection=main_selection, clipboard_text=clipboard_text)


# response for /results (given plane and spw)
@app.route('/multiplayer',methods=['POST', 'GET'])
def multiplayer():
    cost_selected = request.args.get('cost', 'Unlimited')
    plane_selected = request.args.get('planes', 'Random')
    spw_selected = request.args.get('spw', 'Random')
    priority_selected = request.args.get('prioritize', 'noPriority')
    priority_gui = 'y'
    
    main_selection = {}
    clipboard_info = []
    other_messages = {}
    print(cost_selected, plane_selected, spw_selected, priority_selected)
    valid_plane, valid_spw = True, True
    plane_selected_before = plane_selected
    
    if cost_selected not in mp.costLimitList[1:]:
        other_messages["invalid_cost_warning"] = "The cost selected is not in the cost limit list. Defaulting to Unlimited."
        cost_selected = "Unlimited"
        print(other_messages["invalid_cost_warning"])
    else:
        if cost_selected != "Unlimited":
            cost_int = int(cost_selected)
        else:
            cost_int = 10000
        
    if priority_selected not in ["noPriority", "withPriority"]:
        other_messages["invalid_priority_warning"] = "The priority selected is not valid. Defaulting to prioritizing BODY and ARMS over MISC."
        priority_selected = "withPriority"
        print(other_messages["invalid_priority_warning"])
    else:
        if priority_selected == "noPriority":
            priority_gui = 'n'
    
    if plane_selected not in rl.planeListMp + ['Random']:
        valid_plane = False
    spw_name = spw_selected.split(' (')[0]
    allValidWeapons = ['Random'] + [w.split(' ')[0] for w in rl.weaponsMp["a2g"]] + rl.allSPWsMp 
    if spw_name not in allValidWeapons:
        valid_spw = False
        print("Invalid SPW")
        
    if not valid_plane and not valid_spw:
        return render_template('bad.html', errorMsg='Invalid aircraft and SPW selection.')
    if valid_plane and not valid_spw:
        return render_template('bad.html', errorMsg='Invalid SPW selection.')
    if not valid_plane and valid_spw:
        return render_template('bad.html', errorMsg='Invalid aircraft selection.')
    

    if valid_plane and valid_spw:
        if 'Random' not in [plane_selected, spw_selected]:
            spw_selected = mp.checkWeaponMp(spw_selected)
            if not (spw_selected in mp.planeInfo[plane_selected]["spw"]):
                return render_template('bad.html', errorMsg='Incompatible aircraft and SPW selection.')
            

        if plane_selected != "Random":
            if mp.planeInfo[plane_selected]["cost"] > (cost_int - 50):
                planeCost_incomp = (plane_selected + ' (cost: ' +  str(mp.planeInfo[plane_selected]["cost"]) +
                                    ") cannot be used if the cost limit is " + cost_selected + '.')
                print(planeCost_incomp)
                return render_template('bad.html', errorMsg=planeCost_incomp)

        else:  # if plane selected == 'Random'
            if spw_name != 'Random':
                plane_selected = random.choice(rl.planePerSpwMp[spw_selected])

    
    partList, otherMessages = mp.generateMpSelection(verbose=False, guiCost=cost_selected,
                                               guiPlane=plane_selected, guiWeapon=spw_selected,
                                               guiPriority=priority_gui)
    pp.pprint(otherMessages)

    plane_final, spw_final = otherMessages["plane_selection"], otherMessages["spw_selection"]

    main_selection = {'Aircraft': plane_final, 'SPW': spw_final}
    if plane_selected_before == 'Random':
        main_selection['Aircraft'] += ' (Random)'
    if spw_selected == 'Random' and spw_final != 'A2G SPW of choice':
        main_selection['SPW'] += ' (Random)'
    
    clipboard_info.append('Aircraft:\\t' + main_selection['Aircraft'])
    clipboard_info.append('SPW:\\t' + main_selection['SPW'])
    partList.sort(key=lambda x:x[3])

    for idx, part in enumerate(partList):
        clipboard_info.append('Part ' + (str(idx + 1)) + ':\\t' + part)

    clipboard_info.append('Cost:\\t' + str(otherMessages['totalCost']) + '/' + str(cost_selected))
    clipboard_info.append('Prioritizing BODY/ARMS over MISC:\\t' + str(priority_gui == 'y'))
    
    clipboard_text = '\\n'.join(clipboard_info)
    return render_template('multiplayer.html', planesList=rl.planeInfoMp, # rl.planeListMp, 
                           proper_spw_order=rl.proper_spw_orderMp, 
                           planePerSpwMp=rl.planePerSpw_StrMp, spwPerPlaneMp=rl.spwPerPlane_StrMp,
                           proper_spw_orderMp_str=rl.proper_spw_orderMp_Str,
                           parts=partList, main_selection=main_selection, clipboard_text=clipboard_text,
                           additional_info=otherMessages)
    
    
@app.route('/about', methods=['POST', 'GET'])
def about():
    return render_template('about.html')


# # Execute this last to run the app:
# (cell can be interrupted with the stop button above)
# 
# examples: 
# 1. [Unlimited cost / F-16 / GPB / priority](http://127.0.0.1:5000/multiplayer?cost=Unlimited&planes=F-16C&spw=GPB&prioritize=withPriority)  
# 2. [Unlimited cost / random plane / random SPW / priority](http://127.0.0.1:5000/multiplayer?cost=Unlimited&planes=Random&spw=Random&prioritize=withPriority)
# 3. [2500 / random plane / random SPW / priority](http://127.0.0.1:5000/multiplayer?cost=2500&planes=Random&spw=Random&prioritize=withPriority)
# 4. [2000 / random plane / random spw / priority](http://127.0.0.1:5000/multiplayer?cost=2000&planes=Random&spw=Random&priotitize=withPriority)
# 5. [2500/ ADF-11F / MSL/PLSL only](http://127.0.0.1:5000/multiplayer?cost=2500&planes=ADF-11F&spw=MSL%2FPLSL+only)
# 6. [2000 / random plane / HPAA / priority](http://127.0.0.1:5000/multiplayer?cost=2000&planes=Random&spw=HPAA&prioritize=withPriority)
# 7. [2000 / F-15E / random spw / priority](http://127.0.0.1:5000/multiplayer?cost=2000&planes=F-15E&spw=Random&prioritize=withPriority)

# In[ ]:


if __name__ == "__main__":
    # threading.Timer(1, open_browser).start()
    app.run()

